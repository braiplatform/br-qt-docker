#FROM nvidia/cuda:11.3.0-cudnn8-runtime-ubuntu18.04
#FROM nvidia/cuda:11.3.0-cudnn8-devel-ubuntu18.04
FROM tensorflow/tensorflow:devel-gpu  

RUN add-apt-repository "deb http://security.ubuntu.com/ubuntu xenial-security main"
RUN apt-get -y update \
    && apt-get -y autoclean \
    && apt-get -y autoremove

RUN apt-get update && apt-get install -y --no-install-recommends \
	apt-utils \
	nano \
	tcpdump \
	pkg-config \
	software-properties-common \
	g++\
	make \
	qt5-default \
	qtbase5-dev \
	autoconf \
	alien \
	dpkg-dev \
	debhelper \
	build-essential \
	qtbase5-private-dev \
	git \
	wget

# Install the C++ dependencies
RUN apt-get update && apt-get install -y  clang \
	libblas-dev \
	liblapack-dev \
	libtool \
	libboost-all-dev  \
	libgoogle-glog-dev  \
	libevent-dev\
	libssl-dev \
    libgtk2.0-dev \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev\
    libtbb2 \
    libtbb-dev \
    libjpeg-dev \
    libpng-dev \
    libtiff-dev \
    libjasper-dev \
    libeigen3-dev \
    libatlas-base-dev \
    libgomp1 \
    libx264-dev

## install cmake
WORKDIR /opt
RUN cd /opt &&\
	hash -r &&\
	wget https://github.com/Kitware/CMake/releases/download/v3.16.5/cmake-3.16.5.tar.gz &&\
	tar -zxvf cmake-3.16.5.tar.gz &&\
	cd cmake-3.16.5 &&\
	./bootstrap &&\
	make &&\
	make install

# install curl for the people
RUN apt-get remove curl -y
WORKDIR /opt
RUN cd /opt &&\
	git clone https://github.com/whoshuu/cpr.git &&\
	cd cpr  &&\
	git submodule update --init --recursive &&\
	mkdir -p build && cd build &&\
	cmake -D CMAKE_BUILD_TYPE=Debug -D CMAKE_INSTALL_PREFIX=/usr/local .. &&\
	make -j &&\
	make install &&\
	ln -sf /opt/cpr/build/lib/libcurl-d.so /usr/local/lib/ &&\
	ldconfig

# install libmodbus
WORKDIR /opt
RUN cd /opt &&\
	git clone https://github.com/stephane/libmodbus.git \
	&& cd libmodbus &&\
	git submodule update --init --recursive &&\
	./autogen.sh &&\
	./configure --prefix=/usr/local/ \
	&& make install \
	&& ldconfig


# ffmpeg installation
# RUN apt-get update
# RUN apt-get install -y nasm yasm libass-dev libfreetype6-dev libsdl2-dev p11-kit \           
# libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev \                 
# libxcb-xfixes0-dev texinfo zlib1g-dev libchromaprint-dev \         
# frei0r-plugins-dev ladspa-sdk libcaca-dev libcdio-paranoia-dev \        
# libcodec2-dev libfontconfig1-dev libfreetype6-dev libfribidi-dev libgme-dev \      
# libgsm1-dev libjack-dev libmodplug-dev libmp3lame-dev libopencore-amrnb-dev \      
# libopencore-amrwb-dev libopenjp2-7-dev libopenmpt-dev libopus-dev \                
# libpulse-dev librsvg2-dev librubberband-dev librtmp-dev libshine-dev \             
# libsmbclient-dev libsnappy-dev libsoxr-dev libspeex-dev libssh-dev \               
# libtesseract-dev libtheora-dev libtwolame-dev libv4l-dev libvo-amrwbenc-dev \      
# libvpx-dev libwavpack-dev libwebp-dev libx265-dev \      
# libxvidcore-dev libxml2-dev libzmq3-dev libzvbi-dev liblilv-dev \    
# libopenal-dev opencl-dev libjack-dev
# RUN apt-get -y install libbluray-dev libfdk-aac-dev
# ARG FFMPEG_VERSION="4.1"
# ARG PREFIX=/opt/ffmpeg
# RUN echo "ffmpeg: ${FFMPEG_VERSION}"
# WORKDIR /opt
# RUN cd /opt \
# && wget http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.bz2 \
# && tar xvjf ffmpeg-${FFMPEG_VERSION}.tar.bz2
# WORKDIR /opt/ffmpeg-${FFMPEG_VERSION}
# RUN ./configure --enable-gpl --enable-postproc --enable-swscale --enable-avfilter --enable-libmp3lame \
#   --enable-libvorbis --enable-libtheora --enable-libx264 --enable-libspeex --enable-shared --enable-pthreads \
#   --enable-libopenjpeg --enable-nonfree --prefix="${PREFIX}" --extra-cflags="-I${PREFIX}/include" \
#   --extra-ldflags="-L${PREFIX}/lib"
# RUN make 
# RUN make install
# RUN ldconfig

# misc video/image plugins installation
# RUN apt-get update --fix-missing \ 
#     && apt-get install -y gstreamer-plugins-base1.0-dev libgstreamer1.0-dev \
#     && ln -s /usr/lib/x86_64-linux-gnu/glib-2.0/include/glibconfig.h /usr/include/glib-2.0/ 

# Install poco
ARG POCO_VERSION='1.10.1'
ENV POCO_VERSION=${POCO_VERSION}
WORKDIR /opt
RUN cd /opt &&\
	git clone -b master https://github.com/pocoproject/poco.git
WORKDIR /opt/poco
RUN git checkout tags/poco-${POCO_VERSION}-release
RUN	mkdir cmake-build
WORKDIR /opt/poco/cmake-build 
RUN cmake -D CMAKE_INSTALL_PREFIX=/usr/local .. && \
	cmake --build . --config Release && \
	cmake --build . --target install && \
	ldconfig

# dlib installation
RUN add-apt-repository ppa:ubuntu-toolchain-r/test -y
ARG RUNTIME_DEPS='libpng libjpeg-turbo giflib openblas libx11'
ARG BUILD_DEPS='wget unzip cmake build-base linux-headers libpng-dev libjpeg-turbo-dev giflib-dev openblas-dev libx11-dev'
ARG LIB_PREFIX='/usr/local'
ARG DLIB_VERSION='19.22'
ENV DLIB_VERSION=${DLIB_VERSION} \
    LIB_PREFIX=${LIB_PREFIX} \
    DLIB_INCLUDE_DIR='$LIB_PREFIX/include' \
    DLIB_LIB_DIR='$LIB_PREFIX/lib'
WORKDIR /opt
RUN echo "Dlib: ${DLIB_VERSION}" \
RUN rm -rf /usr/local/lib && ln -s /usr/local/lib64 /usr/local/lib
RUN git clone https://github.com/davisking/dlib.git
WORKDIR /opt/dlib
RUN git checkout tags/v${DLIB_VERSION} \
    && mkdir build
WORKDIR /opt/dlib/build
RUN cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=$LIB_PREFIX \
	-D DLIB_NO_GUI_SUPPORT=ON \
	-D DLIB_USE_BLAS=ON \
	-D DLIB_GIF_SUPPORT=ON \
	-D DLIB_PNG_SUPPORT=ON \
	-D DLIB_JPEG_SUPPORT=ON \
	-D DLIB_USE_CUDA=ON ..
RUN make -j $(getconf _NPROCESSORS_ONLN) 
RUN make install
RUN ldconfig 

#install opencv
ARG DEBIAN_FRONTEND=noninteractive
ENV OPENCV_VERSION="4.5.2"
ENV OPENCV_CONTRIB_VERSION="4.5.2" 
ENV ARCH_BIN="7.5"
WORKDIR /opt
RUN set -ex \
    && apt-get -qq update \
    && apt-get -qq install -y --no-install-recommends \
        libhdf5-dev \
        libopenblas-dev \
        libprotobuf-dev \
        libjpeg8 libjpeg8-dev \
        libpng16-16 libpng-dev \
        libtiff5 libtiff-dev \
        libwebp6 libwebp-dev \
        libopenjp2-7 libopenjp2-7-dev \
        tesseract-ocr tesseract-ocr-por libtesseract-dev \
        python3 python3-pip python3-numpy python3-dev 
RUN git clone https://github.com/opencv/opencv_contrib.git
WORKDIR /opt/opencv_contrib
RUN git checkout $OPENCV_CONTRIB_VERSION
WORKDIR /opt
RUN git clone https://github.com/opencv/opencv.git
WORKDIR /opt/opencv
RUN git checkout $OPENCV_VERSION
WORKDIR /opt/opencv/build
RUN cmake \
        -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=/usr/local \
        -D OPENCV_EXTRA_MODULES_PATH=/opt/opencv_contrib/modules \
        -D EIGEN_INCLUDE_PATH=/usr/include/eigen3 \
        -D OPENCV_ENABLE_NONFREE=ON \
        # -D CUDA_ARCH_BIN=${ARCH_BIN} \
		# -D CUDA_ARCH_PTX="" \
		# -D WITH_CUDNN=ON \
		# -D WITH_CUBLAS=ON \
		-D ENABLE_FAST_MATH=ON \
		# -D CUDA_FAST_MATH=ON \
        # -D WITH_CUDA=ON \
        -D WITH_JPEG=ON \
        -D WITH_PNG=ON \
        -D WITH_TIFF=ON \
        -D WITH_WEBP=ON \
        -D WITH_JASPER=ON \
        -D WITH_EIGEN=ON \
        -D WITH_TBB=ON \
        -D WITH_LAPACK=ON \
        -D WITH_PROTOBUF=ON \
        -D WITH_V4L=OFF \
        -D WITH_GSTREAMER=OFF \
        -D WITH_GTK=OFF \
        -D WITH_QT=OFF \
        -D WITH_VTK=OFF \
        -D WITH_OPENEXR=OFF \
        -D WITH_FFMPEG=OFF \
        -D WITH_OPENCL=OFF \
        -D WITH_OPENNI=OFF \
        -D WITH_XINE=OFF \
        -D WITH_GDAL=OFF \
        -D WITH_IPP=OFF \
        -D BUILD_OPENCV_PYTHON3=OFF \
        -D BUILD_OPENCV_PYTHON2=OFF \
        -D BUILD_OPENCV_JAVA=OFF \
        -D BUILD_TESTS=OFF \
        -D BUILD_IPP_IW=OFF \
        -D BUILD_PERF_TESTS=OFF \
        -D BUILD_EXAMPLES=OFF \
        -D BUILD_ANDROID_EXAMPLES=OFF \
        -D OPENCV_GENERATE_PKGCONFIG=YES \
        -D BUILD_DOCS=OFF \
        -D BUILD_ITT=OFF \
        -D INSTALL_PYTHON_EXAMPLES=OFF \
        -D INSTALL_C_EXAMPLES=OFF \
        -D INSTALL_TESTS=OFF ..
RUN make -j$(nproc)
RUN make install 

# RUN apt-get -qq remove -y \
# software-properties-common \
# build-essential cmake \
# libhdf5-dev \
# libprotobuf-dev \
# libjpeg8-dev \
# libpng-dev \
# libtiff-dev \
# libwebp-dev \
# libopenjp2-7-dev \
# libtbb-dev \
# libtesseract-dev \
# python3-dev \

RUN apt-get -qq autoremove \
&& apt-get -qq clean

RUN apt install curl -y
WORKDIR /tensorflow_src
RUN bash tensorflow/lite/tools/make/download_dependencies.sh

WORKDIR /tensorflow_src/tensorflow/lite/tools/make/
RUN bash build_lib.sh

# Install Edge TPU library
## Add Edgetpu Source
RUN echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | tee /etc/apt/sources.list.d/coral-edgetpu.list \
&& curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - \
&& apt-get update

## Install the Edge TPU runtime
RUN apt-get install libedgetpu1-std -y 
## Install dev headers
RUN apt-get install libedgetpu-dev -y 

## install the flatbuffers
WORKDIR /tensorflow_src/tensorflow/lite/tools/make/downloads/flatbuffers/build
RUN cmake  -D CMAKE_INSTALL_PREFIX=/usr/local ..
RUN make -j4
RUN make install
RUN ldconfig



WORKDIR /tensorflow_src/tensorflow/lite/tools/make/downloads/absl/build
RUN cmake .. -DABSL_RUN_TESTS=ON -DABSL_USE_GOOGLETEST_HEAD=ON -DCMAKE_CXX_STANDARD=11  -DCMAKE_INSTALL_PREFIX=/usr/local
RUN cmake --build . --target all
RUN  make install
RUN ldconfig

ENV QT_VERSION v6.1.0
ENV QT_CREATOR_VERSION v4.15.0-rc1

# Build prerequisites
RUN apt-get -y update && apt-get -y install qtbase5-dev \
	libxcb-xinerama0-dev 

# Other useful tools
RUN apt-get -y update && apt-get -y install tmux \
	zip \
	vim


# Simple root password in case we want to customize the container
RUN echo "root:root" | chpasswd
RUN useradd -G video -ms /bin/bash user
# COPY  br-peak-opencv /qt/br-peak-opencv
RUN apt install qtcreator -y
#If you want Qt 5 to be the default Qt version to be used when using development binaries like qmake, install the following package:
RUN apt install qt5-default -y

WORKDIR /opt/
COPY ids-peak-linux-x86-1.2.1.0-64.deb .
RUN apt install ./ids-peak-linux-x86-1.2.1.0-64.deb -y
USER user
WORKDIR /qt