#FROM nvidia/cuda:11.3.0-cudnn8-devel-ubuntu18.04
#FROM tensorflow/tensorflow:devel-gpu  
FROM br-cpp:v2.5


# # Configuration env vars will be set to default values if not defined.
# ENV QT_TOP_DIR="/qt/build"
# #ENV QT_VERSION="master"
# ENV QT_J_LEVEL=$((`nproc`+1))
# ENV QT_GIT_URL="https://code.qt.io/qt/qt5.git"
# ENV QT_WORKING_DIR="qt5"
# ENV QT_INIT_REPOSITORY_OPTS="" 
# #--module-subset=default,-qtwebkit,-qtwebkit-examples,-qtwebengine"
# ENV QT_CREATOR_GIT_URL="https://code.qt.io/qt-creator/qt-creator.git"
# #ENV QT_CREATOR_VERSION="master"
# ENV QT_CREATOR_WORKING_DIR="qt-creator"
# WORKDIR /qt/build/
# RUN git clone $QT_GIT_URL $QT_WORKING_DIR
# WORKDIR /qt/build/qt5
# RUN git checkout $QT_VERSION
# #RUN git submodule update --init --recursive
# RUN perl init-repository -f --module-subset=qtbase #,qtshadertools,qtdeclarative,qtquickcontrols2
# WORKDIR /qt/build/qt5/qt-build
# ENV QT_CONFIGURE_OPTS="-opensource -nomake examples -nomake tests -confirm-license \
# -skip qtconnectivity -skip qtdeclarative -skip qtlocation -skip qtmultimedia -skip qtquick1 \
# -skip qtquickcontrols -skip qtsensors -skip qttools -skip qtwebsockets -skip qtwinextras \
# -skip qtwebchannel -skip qtwebengine -skip qtwebkit -skip qtwebkit-examples  \
# -skip qtquickcontrols2 \
# -no-opengl"
# RUN ../configure $QT_CONFIGURE_OPTS -prefix "/usr/local"
# #RUN ../configure -prefix "/usr/local"

# RUN apt-get -y update && apt-get -y install  libssl1.0-dev zstd
# #RUN make -j$(nproc)
# RUN cmake --build . --parallel 4
# #RUN make install
# RUN cmake --install .
# # Clone and build QT Creator
# WORKDIR /qt/build/
# RUN git clone --recursive $QT_CREATOR_GIT_URL $QT_CREATOR_WORKING_DI
# WORKDIR /qt/build/qt-creator
# RUN git checkout $QT_CREATOR_VERSION
# # Make sure no submodules have the latest version, since this could
# # cause build errors.
# RUN git submodule update --init
# WORKDIR /qt/build/qt-creator/qt-creator-build
# RUN qmake -r ../qtcreator.pro
# RUN make -j$(nproc)





#RUN make install

#ADD build_qt.sh /qt/build/build_qt.sh
#RUN QT_VERSION=$QT_VERSION QT_CREATOR_VERSION=$QT_CREATOR_VERSION /qt/build/build_qt.sh

USER user
WORKDIR /qt

CMD ["bash"]
